# Atrazine

A minimalist rewite of `lolcat` in C.

Feed it text through `stdin`, it will output to `stdout` in rainbow colours. It tries not to break the output when it receives UTF-8 characters and escape sequences. Unlike the orgininal `lolcat`, it's super fast, and very limited in features. 

To install, simply compile it and copy the binary it to a directory in your `PATH` environment variable.

```
gcc atrazine.c -o atrazine -lm -O2 -Wpedantic -Wall -Wextra
```

To use it pipe output of any command to its `stdin`:

```
ls | atrazine
```
